<?php

/**
 * Helper function that returns node id of the connection content.
 */
function syclops_archive_connection_ids($nid) {
 $node_ids = array();
 $node_ids['sessions_id'] = _syclops_archive_get_sessions_id($nid);
 $node_ids['grants_id'] = _syclops_archive_get_grants_id($nid);
 return $node_ids;
}

/**
 * Helper funciton returns connection related message ids.
 */
function _syclops_archive_get_connection_message_id($nid) {
 $node_ids = array();
 $nodes = views_get_view_result('syclops_activities','block_4', $nid);
 if(isset($nodes)) {
  foreach($nodes as $node) {
    array_push($node_ids, $node->mid);
  }
 }
 return $node_ids;
}

/**
 * Helper function returns grant related message ids.
 */
function _syclops_archive_get_grant_message_id($nid) {
 $node_ids = array();
 $nodes = views_get_view_result('syclops_activities','block_5', $nid);
 if(isset($nodes)) {
  foreach($nodes as $node) {
    array_push($node_ids, $node->mid);
  }
 }
 return $node_ids;
}

/**
 * Helper function returns session related messages id.
 */
function _syclops_archive_get_session_message_id($nid) {
 $node_ids = array();
 $nodes = views_get_view_result('syclops_activities','block_6', $nid);
 if(isset($nodes)) {
  foreach($nodes as $node) {
    array_push($node_ids, $node->mid);
  }
 }
 return $node_ids;
}

/**
 * Helper function returns session node id.
 */
function _syclops_archive_get_sessions_id($nid) {
 $node_ids = array();
 $nodes = views_get_view_result('syclops_connection','block_8', $nid);
 if(isset($nodes)) {
  foreach($nodes as $node) {
    array_push($node_ids, $node->nid);
  }
 }
 return $node_ids;
}

/**
 * Helper function returns grants node id.
 */ 
function _syclops_archive_get_grants_id($nid) {
 $node_ids = array();
 $nodes = views_get_view_result('syclops_connection','block_9', $nid);
 if(isset($nodes)) {
  foreach($nodes as $node) {
    array_push($node_ids, $node->nid);
  }
 }
 return $node_ids;
}

